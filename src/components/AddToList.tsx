import React, { useState } from "react";
import { IState as IProps } from "../App";

interface IProps {
    people: Props["people"]
    setPoeople: React.Dispatch<React.SetStateAction<Props["people"]>>
}

const AddToList: React.FC<IProps> = () => {
    const [input, setInput] = useState({
        name: "",
        age: "",
        note: "",
        img: ""
    })

    const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLAreaElement>):void => {
        setInput({
            ...input, 
            [e.target.name]: e.target.value
        })

    }

    const handleClick = () => {
        if(
            !input.name ||
            !input.age ||
            !input.img  
        ){
            return 
        }
        setPoeple([
            ...people, 
            {
                name: input.name,
                age: input.age,
                url: input.img,
                note: input.note
            }
        ])
    }
    return (
        <div className="AddToList">
            <input 
                type="text"
                name="name"
                placeholder="Name"
                className="AddToList-input"
                value={input.name} 
                onChange={handleChange}
            />
            <input 
                type="text"
                name="age"
                placeholder="Age"
                className="AddToList-input"
                value={input.age}
                onChange={handleChange} 
            />
            <input 
                type="text"
                name="img"
                placeholder="Image URL"
                className="AddToList-input"
                value={input.img}
                onChange={handleChange} 
            />
            <textarea 
                placeholder="Note"
            
                className="AddToList-input"
                value={input.note}
                onChange={handleChange} 
            />
            <button
                className="AddToList-btn"
            >
                Add to List
            </button>
        </div>
    )
}

export default AddToList